const rsiIndicator = require("./indicators/rsi");
const http = require("https");

module.exports = strategyOne = async (data) => {
  const BARS = data;
  let signals;
  const RSI = rsiIndicator(data);
  const HttpRequest = (filename, callback) => {

    const value = JSON.stringify({ bars: BARS, rsi: RSI });

    var options = {
      hostname:process.env.HOST_NAME,
      path: "/divergence",
      protocol: "https:",
      method: "POST",
      port: 8081,
      headers: {
        "Content-Type": "application/json",
        "Content-Length": value.length,
      },
    };

    const req = http.request(options, function (resp) {
      resp.on("data", callback);
    });

    req.on("error", function (e) {
      console.log(e);
    });
    
    req.write(value);
    req.end();
    
  }

  HttpRequest("data", async (data) => {
    signals = JSON.parse(JSON.parse(data));
    const regularBull = Object.values(signals.regularBull);
    console.log("Post Request Response From Django:", regularBull);
  });
  return signals
};
