require("dotenv").config();
const strategyOne = require("./strategies/index");
const getBars = require("./controllers/index.js");
const moment = require("moment");


const express = require("express");
const app = express();
const cors = require("cors");
var morgan = require("morgan");



var corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
};

//middleware
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));

// listen on port
const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Server Started on port ${port}`);
});


const tradingBot = async () => {
  const scanMarkets = async () => {

    const start = moment().subtract(1, "days").format();
    const bars = await getBars("BTCUSD", start, 15);

    strategyOne(bars)
      .then((data) => {
        console.log("Strategy One Result", data);
      })
      .catch((err) => {
        console.log("strategyOne - error", err);
      });
  
   
  };
  setInterval(scanMarkets, 120000);
};

tradingBot();
